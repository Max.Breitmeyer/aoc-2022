#!/opt/homebrew/bin/python3
#Day 2 of Advent of Code

def getInput():
    with open('aoc/day2/input.txt', 'r') as f:
        fixedGuide = []
        guide = f.readlines()
        for i in guide:
            i = i.replace('\n', '')
            fixedGuide.append(i.split(' '))
        print(fixedGuide)
        return fixedGuide

def winCondition(round):
    #enemy: A = Rock, B = Paper, C = Scissors
    #me: X = Rock, Y = Paper, Z = scissors

    # A > Z, B > X, C > Y
    """enemyDict = {'A' : 1, 'B' : 2, 'C' : 3}
    myDict = {'X' : 6, 'Y' : 12, 'Z' : 18} 

    enemyChoice = enemyDict.get(round[0])
    myChoice = myDict.get(round[1])

    if(enemyChoice % myChoice == enemyChoice):
        return 0
    elif :
        return 6
    elif (enemyDict.get(round[0]) % myDict.get(round[1]) == 0): #draw
        return 3
        I'll come back to this, just gonna hard code in the mean time"""
    enemyChoice = round[0]
    myChoice = round[1]

    if ((myChoice == 'X' and enemyChoice == 'C') or (myChoice == 'Y' and enemyChoice == 'A') or (myChoice == 'Z' and enemyChoice == 'B')):
        return 6
    elif ((myChoice == 'X' and enemyChoice == 'B') or (myChoice == 'Y' and enemyChoice == 'C')or (myChoice == 'Z' and enemyChoice == 'A')):
        return 0
    else:
        return 3



def getPoints(guide):
    pointTotal = 0
    pointDict = {'X' : 1, 'Y' : 2, 'Z' : 3}
    for i in guide:
        pointTotal += winCondition(i)
        pointTotal += pointDict.get(i[1])
    print(pointTotal)
    return pointTotal

def main():
    array = getInput()
    getPoints(array)

if __name__ == '__main__':
    main()
