#!/opt/homebrew/bin/python3
#Day 2 of Advent of Code

def getInput():
    with open('aoc/day2/input.txt', 'r') as f:
        fixedGuide = []
        guide = f.readlines()
        for i in guide:
            i = i.replace('\n', '')
            fixedGuide.append(i.split(' '))
        print(fixedGuide)
        return fixedGuide

def winCondition(round):
    #enemy: A = Rock, B = Paper, C = Scissors
    #me: X = Lose, Y = Draw, Z = Win

    # A > Z, B > X, C > Y

    enemyChoice = round[0]
    outcome = round[1]

    #outcomes to lead to rock: Paper + Lose, Rock + Draw, Scissors + Win
    #outcomes to lead to paper: Scissors + Lose, Paper + Draw, Rock + Win

    if ((outcome == 'X' and enemyChoice == 'B') or (outcome == 'Y' and enemyChoice == 'A') or (outcome == 'Z' and enemyChoice == 'C')):
        return 1
    elif ((outcome == 'X' and enemyChoice == 'C') or (outcome == 'Y' and enemyChoice == 'B')or (outcome == 'Z' and enemyChoice == 'A')):
        return 2
    else:
        return 3


def getPoints(guide):
    pointTotal = 0
    pointDict = {'X' : 0, 'Y' : 3, 'Z' : 6}
    for i in guide:
        pointTotal += winCondition(i)
        pointTotal += pointDict.get(i[1])
    print(pointTotal)
    return pointTotal

def main():
    array = getInput()
    getPoints(array)

if __name__ == '__main__':
    main()
