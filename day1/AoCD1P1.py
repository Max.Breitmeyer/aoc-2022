#!/opt/homebrew/bin/python3
#Day 1 of Advent of Code

def getInput():
    with open('aoc/day1/day1Input.txt', 'r') as f:
       calories = f.readlines()
       return calories

def seperateElves(calorieArray):
    currentCalories = 0
    elfArray = []
    for i in calorieArray:
        if i != '\n':
            i.replace('\\n', '')
            i = int(i)
            currentCalories += i
        else:
            elfArray.append(currentCalories)
            currentCalories = 0
    return elfArray

def findMax(array):
    curMax = 0
    total = 0
    j = 0
    while j < 3:
        for i in array:
            if i > curMax:
                curMax = i
        print(curMax)
        array.remove(curMax)
        total += curMax
        curMax = 0
        j += 1
    print(total)


def main():
    x = getInput()
    elves = seperateElves(x)
    findMax(elves)

if __name__ == '__main__':
    main()