#!/opt/homebrew/bin/python3
#Day 3 of Advent of Code

"""
One Elf has the important job of loading all of the rucksacks with supplies for the jungle journey. Unfortunately, that Elf didn't quite follow the packing instructions, and so a few items now need to be rearranged.

Each rucksack has two large compartments. All items of a given type are meant to go into exactly one of the two compartments. The Elf that did the packing failed to follow this rule for exactly one item type per rucksack.

The Elves have made a list of all of the items currently in each rucksack (your puzzle input), but they need your help finding the errors. Every item type is identified by a single lowercase or uppercase letter (that is, a and A refer to different types of items).

The list of items for each rucksack is given as characters all on a single line. A given rucksack always has the same number of items in each of its two compartments, so the first half of the characters represent items in the first compartment, while the second half of the characters represent items in the second compartment.

For example, suppose you have the following list of contents from six rucksacks:
"""

#vJrwpWtwJgWrhcsFMMfFFhFp
#jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
#PmmdzqPrVvPwwTWBwg
#wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
#ttgJtRGJQctTZtZT
#CrZsJsPPZsGzwwsLwLmpwMDw

"""
The first rucksack contains the items vJrwpWtwJgWrhcsFMMfFFhFp, which means its first compartment contains the items vJrwpWtwJgWr, while the second compartment contains the items hcsFMMfFFhFp. The only item type that appears in both compartments is lowercase p.
The second rucksack's compartments contain jqHRNqRjqzjGDLGL and rsFMfFZSrLrFZsSL. The only item type that appears in both compartments is uppercase L.
The third rucksack's compartments contain PmmdzqPrV and vPwwTWBwg; the only common item type is uppercase P.
The fourth rucksack's compartments only share item type v.
The fifth rucksack's compartments only share item type t.
The sixth rucksack's compartments only share item type s.
To help prioritize item rearrangement, every item type can be converted to a priority:

Lowercase item types a through z have priorities 1 through 26.
Uppercase item types A through Z have priorities 27 through 52.
In the above example, the priority of the item type that appears in both compartments of each rucksack is 16 (p), 38 (L), 42 (P), 22 (v), 20 (t), and 19 (s); the sum of these is 157.

Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?


"""

"""
Part 2:

Every set of three lines in your list corresponds to a single group, but each group can have a different badge item type. So, in the above example, the first group's rucksacks are the first three lines:

vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
And the second group's rucksacks are the next three lines:

wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
In the first group, the only item type that appears in all three rucksacks is lowercase r; this must be their badges. In the second group, their badge item type must be Z.

Priorities for these items must still be found to organize the sticker attachment efforts: here, they are 18 (r) for the first group and 52 (Z) for the second group. The sum of these is 70.


"""


import string


def getInput():
    with open('aoc/day3/input.txt', 'r') as f:
       packs = f.readlines()
    return [line.strip() for line in packs]

def divideElves(bags):
    elfGroups = []
    for i in range(0, len(bags), 3):
        elfGroups.append([bags[i], bags[i+1], bags[i+2]])
    return elfGroups

def findCommonGroup(elfGroup):
    commonItems = []
    for i in elfGroup:
        commonItems.append(findCommonGroupReturn(i))
    return commonItems

def findCommonGroupReturn(i):
    for j in i[0]:
        if j in i[1] and j in i[2]:
            return j


def divideBags(packArray):
    splitBags = []
    for i in packArray:
        splitPockets = []
        splitIndex = len(i)//2
        pocket1 = i[:splitIndex]
        pocket2 = i[splitIndex:]
        splitPockets.append(pocket1)
        splitPockets.append(pocket2)
        splitBags.append(splitPockets)
    return splitBags


def findCommon(splitBags):
    commonItems = []
    for i in splitBags:
        commonItems.append(findCommonReturn(i))
    return commonItems

def findCommonReturn(i):
    for j in i[0]:
        if j in i[1]:
            return j


def sumPriority(letters):
    sum = 0
    alphabet = list(string.ascii_letters)
    for i in letters:
        if i in alphabet:
            sum += (alphabet.index(i) + 1)
    print(sum)
    return sum
    

def main():
    originalBags = getInput()
    dividedBags = divideBags(originalBags)
    commonItems = findCommon(dividedBags)
    sumPriority(commonItems)
    groupedElves = divideElves(originalBags)
    commonElfItem = findCommonGroup(groupedElves)
    sumPriority(commonElfItem)




if __name__ == '__main__':
    main()